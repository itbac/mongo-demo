package com.itbac.mongodemo.bean;


import com.itbac.mongodemo.mongo.MongoEntity;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/5/1 21:55
 */


public class User extends MongoEntity {

    private static final long serialVersionUID = 1813341756722772162L;


    private String username;
    private String password;

    private String roleCode;


    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public User setRoleCode(String roleCode) {
        this.roleCode = roleCode;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleCode='" + roleCode + '\'' +
                "} " + super.toString();
    }
}
