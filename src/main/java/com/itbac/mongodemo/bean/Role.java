package com.itbac.mongodemo.bean;

import com.itbac.mongodemo.mongo.MongoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 角色
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2021/5/2
 */
@EqualsAndHashCode(callSuper = true) //包括父类的属性
@Data //get、set 方法
@NoArgsConstructor //无参构造
@AllArgsConstructor //全参构造
@Accessors(chain = true) //链式编程
public class Role extends MongoEntity {
    private static final long serialVersionUID = 7714201684305718787L;
    //角色名称
    private String name;
    /**
     * 角色code：
     * root 顶级管理员
     * admin管理员、
     * visitor游客、
     * user注册用户、
     * driver司机、
     * cargoOwner货主
     */
    private String code;
    /**
     * 权限
     * @see Permission
     */
    private List<String> permissions;
    /**
     * 菜单
     * @see Menu
     */
    private List<String> menus;

}
