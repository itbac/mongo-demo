package com.itbac.mongodemo.config;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SnowflakeConfig {

    @Value("${snowflake.workerId}")
    private int workerId;
    @Value("${snowflake.datacenterId}")
    private int datacenterId;

    //雪花算法
    @Bean
    public Snowflake snowflake() {
        System.out.println(String.format("雪花算法,workerId:{%s},workerId：{%s}", workerId, datacenterId));
        return IdUtil.createSnowflake(workerId, datacenterId);
    }
}
