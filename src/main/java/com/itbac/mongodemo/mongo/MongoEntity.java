package com.itbac.mongodemo.mongo;

import org.springframework.data.annotation.Id;

/**
 * mongoDB 数据库实体父类
 * 实现自增主键，设置创建时间，更新时间。
 */
public abstract class MongoEntity {

    @Id
    private long id;

    private String createDate;

    private String updateDate;

    private String createTime;

    private String updateTime;

    public long getId() {
        return id;
    }

    public MongoEntity setId(long id) {
        this.id = id;
        return this;
    }

    public String getCreateDate() {
        return createDate;
    }

    public MongoEntity setCreateDate(String createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public MongoEntity setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getCreateTime() {
        return createTime;
    }

    public MongoEntity setCreateTime(String createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public MongoEntity setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
        return this;
    }
}
