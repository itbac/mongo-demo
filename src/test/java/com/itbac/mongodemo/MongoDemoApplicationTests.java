package com.itbac.mongodemo;

import com.itbac.mongodemo.bean.Role;
import com.itbac.mongodemo.bean.User;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoDemoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    //新增数据
    @Test
    public void test1() {
        User user = new User();
        user.setUsername("测试一下demo");
        user.setPassword("123456");
        user.setRoleCode("admin");
        user.setId(4L);
        User insert = mongoTemplate.insert(user);
        System.out.println(">>>>>>" +insert.toString());
    }
    //更新
    @Test
    public void test2() {
        //设置查询条件  is 表示等于
        Query query = Query.query(Criteria.where("_id").is(1381283797475135488L));
        //设置更新内容
        Update update = Update.update("name", "李白");
        UpdateResult upsert = mongoTemplate.upsert(query, update, User.class);
    }
    //查询
    @Test
    public void test3() {
        //设置查询条件
        User user = mongoTemplate.findOne( Query.query(Criteria.where("username").is("root")), User.class);
        System.out.println(user);
    }
    //删除
    @Test
    public void test4() {
        //设置查询条件
        Query query = Query.query(Criteria.where("_id").is(1381283797475135488L));
        DeleteResult remove = mongoTemplate.remove(query, User.class);

    }
    @Test
    public void test5(){
        List<User> all = mongoTemplate.findAll(User.class);
        if (!CollectionUtils.isEmpty(all)){
            all.forEach(s -> System.out.println(">>>>>>"+s.toString()));
        }else {
            System.out.println(">>>>>>查不到数据。。。。。。。。。");
        }
    }
    @Test
    public void test6(){
        Role role = new Role()
                .setName("管理员")
                .setCode("admin");
        role.setId(3L);
        Role role1 = mongoTemplate.save(role);
        System.out.println(">>>>>> "+role1.toString());


    }


}
